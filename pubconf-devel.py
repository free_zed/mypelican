#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

# If your site is available via HTTPS, make sure SITEURL begins with https://
SITEURL = 'http://free_zed.gitlab.io/mypelican'

FEED_ALL_ATOM = 'feeds/all.atom.xml'

LINKS = (('Archives', '/mypelican/articles/'),
         ('Tags', '/mypelican/tags/'),
         ('Mentions légales', '/mypelican/pages/mentions-legales/'),)

# URL of the repo to link build commit
REPO_COMMIT_URL = 'https://gitlab.com/free_zed/mypelican/commit'
