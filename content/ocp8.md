Title: Le gras c'est la vie !
Date: 2018-11-01 11:11
Category: Réalisations
Summary: Proposer un client web pour OpenFoodFacts qui propose des aliments de substitution
Tags: django, api-rest, heroku, tdd, extrem-programming, openfoodfacts, postgresql, agile, git, python,


La startup **Pur Beurre**, avec laquelle vous avez [déjà travaillé][p5], souhaite développer une plateforme web qui permettra de trouver un substitut à un aliment considéré comme _trop gras, trop sucré, trop salé_.

- La recherche **ne doit pas** s’effectuer en AJAX
- Interface responsive
- Compte utilisateur
    * création de compte (mail et mot de passe)
    * sans possibilité de changer son mot de passe
- BDD `PostgreSql`
- TDD (mocks pour les API)
- Suivre la PEP 8
- Méthodologie agile

---

Voir le code hébergé sur [Github][ocp8]


[ocp8]: https://github.com/freezed/ocp8/tree/v0.1
[p5]: {filename}/ocp5.md
