Title: Création & conception du &laquo;Waste Watcher&raquo;
Date: 2017-11-20 11:11
Category: Réalisations
Slug: parcours-innovation-collectif-arche-innovateur
Status: published
Summary: De la feuille blanche au prototype, la création d'un outils de gestion des déchets.
Tags: innovation, collectif, iot, arduino, impression-3d

_L'Arche aux Innovateurs_ est un parcours immersif en innovation collective que j'ai réalisé en fin d'année 2017.

Le principe est le suivant :

* une durée  de 10 semaines
* un atelier pour chacun des 9 thèmes

![Plan des étapes][etapes]

* une équipe réduite aux profils variés (et qui ne se connaissent pas)

Pour **faire ensemble**, sur le thème imposé **`les déchets`** et à partir d'une **`feuille blanche`** :

* 1 plan et des supports de communication

![Cas d'utilisations][desc]

* 1 prototype

![Vue du prototype][proto]

* 1 planification financière

Nous avons conçu et prototypé : un _pèse déchets connectée_ : Le **Waste Watchers**.

Plus d'information sur le [blog des MacFivers][blog], avec l'historique du projet :

[![Logo Waste Watchers][logo]][blog]

[logo]: https://image.jimcdn.com/app/cms/image/transf/dimension=320x10000:format=jpg/path/s0b3477ee27b20e29/image/i82f083d64137c7c1/version/1509209533/image.jpg "Logo Waste Watchers"
[proto]: https://image.jimcdn.com/app/cms/image/transf/dimension=500x10000:format=png/path/s0b3477ee27b20e29/image/i382f53ce3e849fce/version/1523006769/image.png "Le prototype"
[desc]: https://image.jimcdn.com/app/cms/image/transf/dimension=500x10000:format=png/path/s0b3477ee27b20e29/image/if3252abcce73f930/version/1523006830/image.png "Cas d'utilisations"
[etapes]: https://image.jimcdn.com/app/cms/image/transf/dimension=500x1024:format=png/path/s54d83bcc40df1c7b/image/icf07739a9deead91/version/1504130055/image.png "Plan des étapes"
[blog]: https://archeauxinnovateurs-macfiver.jimdo.com/blog/#cc-m-header-7208044154
