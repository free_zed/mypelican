Title: Tentative de helloworld sur AWS Lambda avec AWS CLI
Date: 2019-02-10 21:05
Category: Bloc-notes
Summary: Essayer de déployer un «helloworld.py» sur AWS Lambda avec AWS CLI
Status: Published
Tags: aws, aws-lambda, serverless, devops, api-gateway, cli,

1. Create AWS account
1. Create AWS IAM role

    - **name** : `lambda-apigateway-role`
    - **Desc** : `Allows Lambda functions to call AWS services on your behalf.`
    - **Trusted entities** : `Service AWS: lambda`

1. Install CLI
1. create `helloworld.py`
1. [Create API gateway][aws-apigateway]

```
user@machine $ aws apigateway create-rest-api --name 'HelloWorld API'
{
    "items": [
        {
            "id": "tbvck78wh9",
            "name": "HelloWorld API",
            "createdDate": 1549840237,
            "apiKeySource": "HEADER",
            "endpointConfiguration": {
                "types": [
                    "EDGE"
                ]
            }
        }
    ]
}

user@machine $ API="tbvck78wh9"
user@machine $ aws apigateway get-resources --rest-api-id $API
{
    "items": [
        {
            "id": "7valik3ze5",
            "path": "/"
        }
    ]
}

user@machine $ PARENT_API=7valik3ze5
user@machine $ aws apigateway create-resource --rest-api-id $API --parent-id $PARENT_API --path-part hello
{
    "id": "e86q4n",
    "parentId": "7valik3ze5",
    "pathPart": "hello",
    "path": "/hello"
}

user@machine $ RESOURCE_HELLO=e86q4n
user@machine $ aws apigateway create-resource --rest-api-id $API --parent-id $RESOURCE_HELLO --path-part '{param}'
{
    "id": "7jptx2",
    "parentId": "e86q4n",
    "pathPart": "{param}",
    "path": "/hello/{param}"
}

user@machine $ aws apigateway put-method --rest-api-id $API --resource-id $RESOURCE_HELLO --http-method GET --authorization-type "NONE"
{
    "httpMethod": "GET",
    "authorizationType": "NONE",
    "apiKeyRequired": false
}

user@machine $ aws apigateway put-method --rest-api-id $API --resource-id 7jptx2 --http-method GET --authorization-type "NONE" --request-parameters method.request.path.param=true
{
    "httpMethod": "GET",
    "authorizationType": "NONE",
    "apiKeyRequired": false,
    "requestParameters": {
        "method.request.path.param": true
    }
}

user@machine $ aws apigateway put-method-response --rest-api-id $API --resource-id $RESOURCE_HELLO --http-method GET --status-code 200
{
    "statusCode": "200"
}

user@machine $ aws apigateway put-method-response --rest-api-id $API --resource-id 7jptx2 --http-method GET --status-code 200
{
    "statusCode": "200"
}

user@machine $ aws apigateway put-integration --rest-api-id $API --resource-id $RESOURCE_HELLO --http-method GET --type HTTP --integration-http-method GET --uri 'http://helloworld-demo-endpoint.execute-api.com/helloworld/hello'
{
    "type": "HTTP",
    "httpMethod": "GET",
    "uri": "http://helloworld-demo-endpoint.execute-api.com/helloworld/hello",
    "connectionType": "INTERNET",
    "passthroughBehavior": "WHEN_NO_MATCH",
    "timeoutInMillis": 29000,
    "cacheNamespace": "e86q4n",
    "cacheKeyParameters": []
}

user@machine $ aws apigateway put-integration --rest-api-id $API --resource-id 7jptx2 --http-method GET --type HTTP --integration-http-method GET --uri 'http://helloworld-demo-endpoint.execute-api.com/helloworld/hello/{param}' --request-parameters '{"integration.request.path.param":"method.request.path.param"}'
{
    "type": "HTTP",
    "httpMethod": "GET",
    "uri": "http://helloworld-demo-endpoint.execute-api.com/helloworld/hello/{param}",
                {api-id}.execute-api.{region}.amazonaws.com

    "connectionType": "INTERNET",
    "requestParameters": {
        "integration.request.path.param": "method.request.path.param"
    },
    "passthroughBehavior": "WHEN_NO_MATCH",
    "timeoutInMillis": 29000,
    "cacheNamespace": "7jptx2",
    "cacheKeyParameters": []
}

user@machine $ aws apigateway put-integration-response --rest-api-id $API --resource-id $RESOURCE_HELLO --http-method GET --status-code 200 --selection-pattern ""
{
    "statusCode": "200",
    "selectionPattern": ""
}

user@machine $ aws apigateway put-integration-response --rest-api-id $API --resource-id 7jptx2 --http-method GET --status-code 200 --selection-pattern ""
{
    "statusCode": "200",
    "selectionPattern": ""
}

user@machine $ aws apigateway create-deployment --rest-api-id $API --stage-name test --stage-description 'Test stage' --description 'First deployment'
{
    "id": "mvcivh",
    "description": "First deployment",
    "createdDate": 1549845704
}

https://tbvck78wh9.execute-api.eu-west-3.amazonaws.com/test/hello/
          {api-id}.execute-api.{region}.amazonaws.com
```

---

[aws-zones]: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html
[aws-iam]: https://console.aws.amazon.com/iam/home#/home
[aws-iam-role]: https://docs.aws.amazon.com/fr_fr/IAM/latest/UserGuide/id_roles_create_for-user.html
[aws-lambda-home]: https://console.aws.amazon.com/lambda/home
[aws-lambda-http]: https://docs.aws.amazon.com/fr_fr/lambda/latest/dg/with-on-demand-https.html
[aws-config-lambda-role]: https://stackoverflow.com/a/37499226/6709630
[aws-config-cli]: https://docs.aws.amazon.com/fr_fr/cli/latest/userguide/cli-chap-configure.html
[aws-config-cli-old]: https://docs.aws.amazon.com/fr_fr/lambda/latest/dg/gettingstarted-tools.html
[aws-credential]: https://aws.amazon.com/fr/blogs/security/a-new-and-standardized-way-to-manage-credentials-in-the-aws-sdks/
[aws-apigateway]: https://docs.aws.amazon.com/fr_fr/apigateway/latest/developerguide/create-api-using-awscli.html
[aws-]:
