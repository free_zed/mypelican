Title: CI/CD d'un projet Django sur VPS
Date: 2018-11-24 11:11
Category: Réalisations
Status: Published
Summary: Intégration et déploiement continue d'un projet Django sur un VPS Digital Ocean
Tags: django, travis, ci, cd, heroku, tdd, openfoodfacts, postgresql, devops, debian, sentry, admin, cli, git, python,

## Historique

* [Livraison][v01] du [projet initial][p8], conformément au [cahier des charges][cdc]
* [Livraison][v03] d'un [1er lot d'évolutions][p11]

## Missions

* Mettre en place une [Intégration Continue][wikici](CI) avec [Travis CI][ci]
* Déploiement de l'[environnement de production][wikienv] sur un [VPS][wikivps] _Digital Ocean_ via [CLI][wikicli]
* Déploiement automatisé dans un [environnement de qualification][wikienv] (_staging environment_) via [Heroku][herokuapp] après [réussite des tests][ci]
* Suivre l'activité :
    - du serveur avec le monitoring _Digital Ocean_
    - de l'application avec [Sentry][sentry]
* Utiliser [`cron`][cron] pour automatiser une [tâche de maintenance][issue64] sur le serveur

---

Voir le code hébergé sur [Github][ocp10]


[ocp10]: https://github.com/freezed/ocp8/tree/v0.4
[cdc]: https://github.com/freezed/ocp8/blob/v0.1/README.md#cahier-des-charges
[ci]: https://travis-ci.com/freezed/ocp8/builds "Liens vers l'historique des builds sur le site Travis CI"
[cron]: https://fr.wikipedia.org/wiki/Cron "Lien vers la page &laquo;cron&raquo; sur wikipedia"
[herokuapp]: https://ocp8-1664.herokuapp.com/
[issue64]: https://github.com/freezed/ocp8/issues64
[p10]: https://github.com/freezed/ocp8/projects/3
[p11]: https://github.com/freezed/ocp8/projects/2
[p5]: https://github.com/freezed/ocp5#pydev-projet-5
[p8]: https://github.com/freezed/ocp8/projects/1
[prod]: http://68.183.223.134/
[purbeurre]: http://68.183.223.134/#about
[readmev04]: https://github.com/freezed/ocp8/blob/v0.4/README.md#contexte
[screenshot]: https://github.com/freezed/ocp8/blob/v0.4/doc/img/
[sentry]: https://sentry.io/ "Lien vers le site Sentry.io"
[v01]: https://github.com/freezed/ocp8/releases/tag/v0.1
[v03]: https://github.com/freezed/ocp8/releases/tag/v0.3
[v04]: https://github.com/freezed/ocp8/releases/tag/v0.4
[wikici]: https://fr.wikipedia.org/wiki/Int%C3%A9gration_continue "Lien vers la page &laquo;Intégration continue&raquo; sur wikipedia"
[wikicli]: https://fr.wikipedia.org/wiki/Command-line_interface "Lien vers la page &laquo;Command Line Interface&raquo; sur wikipedia"
[wikidjango]: https://fr.wikipedia.org/wiki/Django_(framework)  "Lien vers la page &laquo;Django (framework)&raquo; sur wikipedia"
[wikienv]: https://fr.wikipedia.org/wiki/Environnement_(informatique) "Lien vers la page &laquo;Environnement (informatique)&raquo; sur wikipedia"
[wikiframe]: https://fr.wikipedia.org/wiki/Framework "Lien vers la page &laquo;Framework&raquo; sur wikipedia"
[wikipaas]: https://fr.wikipedia.org/wiki/Plate-forme_en_tant_que_service "Lien vers la page &laquo;Plate-forme en tant que service&raquo; sur wikipedia"
[wikivps]: https://fr.wikipedia.org/wiki/Serveur_d%C3%A9di%C3%A9_virtuel "Lien vers la page     &laquo;Serveur dédié virtuel&raquo; sur wikipedia"
[pitch]: https://gitpitch.com/freezed/ocp8/v0.4?p=doc
