Title: Parcours dev python : projet final
Date: 2018-12-20 11:11
Category: Réalisations
Status: Published
Summary: Un outils de suivi de clientèle pour traivailleurs indépendants
Tags: django, travis, ci, cd, heroku, tdd, sentry, postgresql, extrem-programming, devops, git, python,


## Note d'intention

Valérie et Serge sont travailleurs indépendants et pour leurs activités respectives ils sont à la recherche d'un outil qui leur permettrait de gérer aisément leur _réseau de contacts_ : prospection, suivi, opportunités etc.

## Spécifications

* service en ligne ([SaaS][wikisaas]), en gardant à l'esprit qu'un usage _hors réseau_ sera certainement implémenté à l'avenir
* les données seront réparties dans différents _objets_ :
    - des `contacts` auxquels seront associés des `affaires`
    - aux `affaires` et `contacts` pourront être associés à des `notes`
    - des `étiquettes` permettront une organisation souple et transversale de ces informations en s'associant à tout ou partie des objets définis ci dessus. Une étiquette pourrait représenter : un _groupe de contact_, une _entreprise_, un _secteur professionnel_, un _statut_, etc.
* les `contacts` seront cloisonnés à l'`utilisateur` qui les aura créés, en gardant à l'esprit qu'un partage des `contacts` entre les `utilisateurs` du service sera implémenté à l'avenir
* import de `contacts` existant :
    - individuel (formulaire)
    - en lot, préférence pour les formats [Vcard][wikivcf] et/ou [CSV][wikicsv]
    - connection à un serveur [CardDAV][wikidav]

---

Voir le code hébergé sur [Github][ocp13]


[ocp13]: https://github.com/freezed/ocp13
[wikicsv]: https://fr.wikipedia.org/wiki/Comma-separated_values "Lien vers la page &laquo;Comma-separated_values&raquo; sur Wikipedia"
[wikidav]: https://fr.wikipedia.org/wiki/CardDAV "Lien vers la page &laquo;CardDAV&raquo; sur Wikipedia"
[wikitdd]: https://fr.wikipedia.org/wiki/Test_driven_development "Lien vers la page &laquo;Test driven development&raquo; sur wikipedia"
[wikivcf]: https://fr.wikipedia.org/wiki/VCard "Lien vers la page &laquo;VCard&raquo; sur Wikipedia"
[wikisaas]: https://fr.wikipedia.org/wiki/Logiciel_en_tant_que_service "Lien vers la page &laquo;Logiciel en tant que service&raquo; sur wikipedia"
