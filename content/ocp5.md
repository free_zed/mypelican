Title: CLI pour l'API Open Food Facts
Date: 2018-08-09 11:11
Category: Réalisations
Slug: client-python-cli-api-openfoodfacts
Status: published
Summary: Client en ligne de commande manipulant les données d'OpenFoodFacts
Tags: api-rest, cli, mariadb, openfoodfacts, python, git,


- Recherche d'aliments alternatif dans la base [Open Food Facts][off]
- Affichage de fiches produits
- L'utilisateur :
    * Interagit avec le système dans le terminal
    * Enregistre les produits pour les retrouver plus tard
- L'utilisateur choisi en tapant des un chiffres

---

Voir le code hébergé sur [Github][ocp5]


[off]: https://fr.openfoodfacts.org/
[ocp5]: https://github.com/freezed/ocp5/
