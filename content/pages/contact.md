Title: Contact
Lang: fr
Slug: contact
Status: published
Summary: Contactez moi!

Mon mail : [pro@zind.fr](mailto:pro@zind.fr)

Mon tel : [+336 59 44 28 36](tel:+33659442836)

Mon _CV_ est téléchargeable en suivant [ce lien][cvdown] et consultable [en ligne ici][cvview].

Vous pouvez regarder et participer à mes contribution sur les réseaux suivants :

[cvview]: https://freezed.me/index.php/s/VM3F2r0wNXsOJDj#pdfviewer
[cvdown]: https://freezed.me/index.php/s/VM3F2r0wNXsOJDj/download#
