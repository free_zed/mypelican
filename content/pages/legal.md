Title: Mentions légales
Status: hidden
Summary: Mentions Légales

###### Lexique :

- **Utilisateur** : Internaute se connectant, utilisant le site &laquo;`pro[zind]`&raquo;.
- **Informations personnelles** : « les informations qui permettent, sous quelque forme que ce soit, directement ou non, l'identification des personnes physiques auxquelles elles s'appliquent » (article 4 de la loi n° 78-17 du 6 janvier 1978).

### 1. Présentation du site.

En vertu de l'article 6 de la loi n° 2004-575 du 21 juin 2004 pour la confiance dans l'économie numérique, il est précisé aux utilisateurs du site &laquo;`pro[zind]`&raquo; l'identité des différents intervenants dans le cadre de sa réalisation et de son suivi :

- **Propriétaire** : Frédéric Zind – Rhônalpie – France
- **Créateur** : [Frédéric Zind](http://pro.zind.fr)
- **Responsable publication** : Frédéric Zind – `pro [at] zind.fr`
    - Le responsable publication est une personne physique.
- **Webmaster** : Frédéric Zind – `pro [at] zind.fr`
- **Hébergeur** : [Gitlab Inc.](https://gitlab.com/) – San Francisco - USA
- **Crédits** :
    - Le modèle de mentions légales est offert par `Subdelirium.com` [Modèle de mentions légales](https://www.subdelirium.com/generateur-de-mentions-legales/)
    - Le _Favicon_ est une une création de : [eeme158 team](https://commons.wikimedia.org/wiki/File:FileZilla_logo.svg) (uploaded by botg) [GPL](http://www.gnu.org/licenses/gpl.html), via Wikimedia Commons
    - Le site est généré avec [Pelican](http://getpelican.com/)
    - Le thème dérivé du travail de [Smashing Magazine](https://www.smashingmagazine.com/2009/08/designing-a-html-5-layout-from-scratch/)

### 2. Conditions générales d’utilisation du site et des services proposés.

L’utilisation du site &laquo;`pro[zind]`&raquo; implique l’acceptation pleine et entière des conditions générales d’utilisation ci-après décrites. Ces conditions d’utilisation sont susceptibles d’être modifiées ou complétées à tout moment, les utilisateurs du site &laquo;`pro[zind]`&raquo; sont donc invités à les consulter de manière régulière.

Ce site est normalement accessible à tout moment aux utilisateurs. Une interruption pour raison de maintenance technique peut être toutefois décidée par Frédéric Zind, qui s’efforcera alors de communiquer préalablement aux utilisateurs les dates et heures de l’intervention.

Le site &laquo;`pro[zind]`&raquo; est mis à jour régulièrement par Frédéric Zind. De la même façon, les mentions légales peuvent être modifiées à tout moment : elles s’imposent néanmoins à l’utilisateur qui est invité à s’y référer le plus souvent possible afin d’en prendre connaissance.

### 3. Description des services fournis.

Frédéric Zind s’efforce de fournir sur le site &laquo;`pro[zind]`&raquo; des informations aussi précises que possible. Toutefois, il ne pourra être tenue responsable des omissions, des inexactitudes et des carences dans la mise à jour, qu’elles soient de son fait ou du fait des tiers partenaires qui lui fournissent ces informations.

Tous les informations indiquées sur le site &laquo;`pro[zind]`&raquo; sont données à titre indicatif, et sont susceptibles d’évoluer. Par ailleurs, les renseignements figurant sur le site &laquo;`pro[zind]`&raquo; ne sont pas exhaustifs. Ils sont donnés sous réserve de modifications ayant été apportées depuis leur mise en ligne.

### 4. Limitations contractuelles sur les données techniques.

Le site n'utilise pas la technologie JavaScript.

Le site Internet ne pourra être tenu responsable de dommages matériels liés à l’utilisation du site. De plus, l’utilisateur du site s’engage à accéder au site en utilisant un matériel récent, ne contenant pas de virus et avec un navigateur de dernière génération mis-à-jour

### 5. Propriété intellectuelle et contrefaçons.

Frédéric Zind est propriétaire des droits de propriété intellectuelle ou détient les droits d’usage sur tous les éléments accessibles sur le site, notamment les textes, images, graphismes, logo, icônes, sons, logiciels.

Toute reproduction, représentation, modification, publication, adaptation de tout ou partie des éléments du site, quel que soit le moyen ou le procédé utilisé, est soumise au respect de la licence [Copyleft CC-BY-SA](https://creativecommons.org/licenses/by-sa/2.0/deed.fr "Licence du contenu").

Toute exploitation non autorisée du site ou de l’un quelconque des éléments qu’il contient sera considérée comme constitutive d’une contrefaçon et poursuivie conformément aux dispositions des articles L.335-2 et suivants du Code de Propriété Intellectuelle.

### 6. Limitations de responsabilité.

Frédéric Zind ne pourra être tenue responsable des dommages directs et indirects causés au matériel de l’utilisateur, lors de l’accès au site &laquo;`pro[zind]`&raquo;, et résultant soit de l’utilisation d’un matériel ne répondant pas aux spécifications indiquées au point 4, soit de l’apparition d’un bug ou d’une incompatibilité.

Frédéric Zind ne pourra également être tenue responsable des dommages indirects (tels par exemple qu’une perte de marché ou perte d’une chance) consécutifs à l’utilisation du site &laquo;`pro[zind]`&raquo;.

Aucun espaces interactif n'est proposé aux utilisateurs.

### 7. Gestion des données personnelles.

En France, les données personnelles sont notamment protégées par la loi n° 78-87 du 6 janvier 1978, la loi n° 2004-801 du 6 août 2004, l'article L. 226-13 du Code pénal et la Directive Européenne du 24 octobre 1995.

Frédéric Zind ne collecte **aucune information personnelle** relatives à l'utilisateur par le site &laquo;`pro[zind]`&raquo; et par conséquent le site n'est pas déclaré à la CNIL.

### 8. Liens hypertextes et cookies.

Le site &laquo;`pro[zind]`&raquo; contient un certain nombre de liens hypertextes vers d’autres sites, mis en place avec l’autorisation de Frédéric Zind. Cependant, Frédéric Zind n’a pas la possibilité de vérifier le contenu des sites ainsi visités, et n’assumera en conséquence aucune responsabilité de ce fait.

La navigation sur le site &laquo;`pro[zind]`&raquo; **n’installe pas de cookie** sur l’ordinateur de l’utilisateur. Un cookie est un fichier de petite taille, qui ne permet pas l’identification de l’utilisateur, mais qui enregistre des informations relatives à la navigation d’un ordinateur sur un site.

Le refus d’installation d’un cookie est une pratique encouragée par Frédéric Zind, le cas échéant, il vous encourage à les supprimer une fois votre navigation terminée sur un site.

### 9. Droit applicable et attribution de juridiction.

Tout litige en relation avec l’utilisation du site &laquo;`pro[zind]`&raquo; est soumis au droit français. Il est fait attribution exclusive de juridiction aux tribunaux compétents de Lyon.

### 10. Les principales lois concernées.

Loi n° 78-17 du 6 janvier 1978, notamment modifiée par la loi n° 2004-801 du 6 août 2004 relative à l'informatique, aux fichiers et aux libertés.

Loi n° 2004-575 du 21 juin 2004 pour la confiance dans l'économie numérique.
