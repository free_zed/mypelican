Title: Accueil
Slug: accueil
Status: hidden
Summary: Accueil

## Bonjour !

#### Je suis _Frédéric Zind_ et je te souhaite la bienvenue sur cette page.

#### Professionnel en **maintenance des véhicules** et en **développement Python**, je partage ici des [notes][notes] et ce que [j'ai pu faire][real], principalement dans les domaines du *développement informatique* et de *l'administration système*.


[notes]: /realisations/
[real]: /bloc-notes/
