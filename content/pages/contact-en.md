Title: Contact
Lang: en
Slug: contact
Status: published
Summary: Contact me!

Email : [pro@zind.fr](mailto:pro@zind.fr)

Phone : [+336 59 44 28 36](tel:+33659442836)

My _resume_ is downloadable [here][cvdown] or viewable [here][cvview].

Have a look and push a commit to my contributions on these networks :

[cvview]: https://freezed.me/index.php/s/ljNhdrNNyVx8DAc#pdfviewer
[cvdown]: https://freezed.me/index.php/s/ljNhdrNNyVx8DAc/download#
