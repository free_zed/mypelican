Title: Install wxPython on Debian/Buster
Date: 2019.03.11 11:55
Summary: Install notes to set-up a wxPython «HelloWorld» on Debian/Buster
Category: Bloc-notes
Lang: en
Slug: wxpython-installation-debian-buster
Status: Published
Tags: wxpython, wxwidget, debian, admin, gui, helloworld, python, dev

**[`wxPython`][wxpy]** is a cross-platform GUI toolkit implementation of **[`wxWidget`][wxw]** for the Python language to create truly native user interfaces applications.

## wxPython installation

* `sudo apt install build-essential dpkg-dev freeglut3-dev libasound2-doc libgstreamer1.0-dev libgtk-3-dev libjpeg-dev liblzma-doc libnotify-dev libsdl1.2-dev libsm-dev libtiff-dev libwxgtk3.0-gtk3-dev libwxgtk-webview3.0-gtk3-dev libxtst-dev python3.7-dev`
* `virtualenv --python python3.7 .venv`
* `source .venv/bin/activate`
* `pip install wxPython --build ~/tmp`

## `HelloWorld` [source][wiki-wxpy]

```python
#!/usr/bin/env python3
# coding:utf-8
import wx

# Création d'un nouveau cadre, dérivé du wxPython 'Frame'.
class TestFrame(wx.Frame):
    def __init__(self, parent, ID, title):
        wx.Frame.__init__(self, parent, -1, title, pos=(-1, -1), size=(200, 100))

        # À l'intérieur du cadre, créer un panneau..
        panel = wx.Panel(self, -1)

        # Créer un texte dans le panneau
        texte = wx.StaticText(panel, -1, "Bonjour tout le monde!", wx.Point(10, 5), wx.Size(-1, -1))

        # Créer un bouton dans le panneau
        bouton = wx.Button(panel, -1, "Cliquez-moi!",  wx.Point(10, 35), wx.Size(-1, -1))
        # lier le bouton à une fonction:
        self.Bind(wx.EVT_BUTTON, self.creerDiag, bouton)

    # fonction qui affiche une boîte de dialogue
    def creerDiag(self, event):
        dlg = wx.MessageDialog(self, "Merci de m'avoir cliqué, ça fait du bien.",
          "Merci!", wx.ICON_EXCLAMATION | wx.YES_NO | wx.CANCEL)
        dlg.ShowModal()
        dlg.Destroy()


# Chaque application wxWidgets doit avoir une classe dérivée de wx.App
class TestApp(wx.App):
    def OnInit(self):
        frame = TestFrame(None, -1, "Test")
        self.SetTopWindow(frame)
        frame.Show(True)
        return True

if __name__ == '__main__':
    app = TestApp(0)
    app.MainLoop()
```

### Tadaaa !

![HelloWorld result image][img]

Notes :
-------

* I had troubles with `python3.6` (but I forgot notice what…)
* About 4.2G of free space is needed in `/tmp` : [I had to set a user-land build-place for pip][pip]

---
[pip]: https://pip.pypa.io/en/stable/reference/pip_install/#cmdoption-b
[wiki-wxpy]: https://fr.wikipedia.org/wiki/WxPython#Exemple
[img]: {static}/img/20190311-helloworld-wxpython-mate-dark.png
[wxw]: https://www.wxwidgets.org/
[wxpy]: https://wxpython.org/
