Title: GrandPy Bot à la mode «serverless»
Date: 2019-03-16 23:34
Summary: Évolution du projet 7 réalisé durant le parcours OpenClassrooms avec : Zappa, AWS Lambda & Gitlab-CI/Pages.
Category: Réalisations
Lang: fr
Slug: grandpy-bot-zappa-flask-lambda-gitlab
Status: Published
Tags: zappa, flask, aws, aws-lambda, gitlab-ci, gitlab-pages, devops, ci, cd, ci-cd, git, bot, api-gateway, python

Il s'agissait d'un [projet étudiant][oc] précédemment hébergé sur [Github][ocp7]. Je l'ai mis à jour pour jouer avec :

* [Zappa][zappa]
* [Flask][flask]
* [AWS Lambda][awslmbd]
* [Gitlab-ci][glci]

Un papy-robot qui parle de souvenirs géographiques et citant Wikipedia.

Échangez donc [quelques phrases][prod] avec lui (^_^)

Installez le vôtre !
--------------------

L'exécution a besoin de quelques variables d'environnement :

- `EMAIL_ADDRESS` (Nominatim API)
- `MAPBOX_API_KEY` (Mapbox API)
- `GOO_API_KEY` (not used [but needed][issue51])

### Localement :

Il fonctionne comme une simple application mono-page [flask][flask] :

- [forkez-le][fork]
- mettez en place un environement virtuel : `virtualenv .venv`
- installez les dépendances : `pip install -r requirements.txt`
- executez locallement : `python run.py`

### Sur Gitlab :

Créez un profil [AWS IAM][awsiam] sur **votre compte AWS** et ajoutez des variables d'environnement valides dans `https://gitlab.com/<namespace>/grandpy/settings/ci_cd` :

Repoussez le et le script d'intégration continue :

- déployera le code du _chat_ sur [AWS Lambda][awslmbd] avec l'aide magique de [Zappa][zappa]
- mettra à jour le [JavaScript][js] avec l'URL _AWS API Gateway_ pointant vers _Lambda_
- génèrera la page d'accueil HTML (avec un [script flask][genstat])
- déployera les fichiers statiques sur _gitlab-pages_ (HTML, images & CSS)

...et voilà, vous pouvez maintenant discuter avec _GrandPy_ sur `https://<namespace>.gitlab.io/grandpy/` (- ;

---

N'hésitez pas à me faire part de vos commentaires via les [tickets][issues].

[awsiam]: https://github.com/Miserlou/Zappa/blob/master/example/policy/deploy.json
[awslmbd]: https://console.aws.amazon.com/lambda/
[flask]: http://flask.pocoo.org/
[fork]: https://gitlab.com/free_zed/grandpy/forks/new
[genstat]: https://gitlab.com/free_zed/grandpy/blob/master/genstat.py
[glci]: https://gitlab.com/free_zed/grandpy/blob/master/.gitlab-ci.yml
[issue51]: https://gitlab.com/free_zed/grandpy/issues/
[issues]: https://gitlab.com/free_zed/grandpy/issues/new
[js]: https://gitlab.com/free_zed/grandpy/tree/master/static/js/ask.js
[oc]: {filename}/ocp7.md
[ocp7]: https://github.com/freezed/ocp7
[prod]: https://free_zed.gitlab.io/grandpy
[zappa]: https://github.com/Miserlou/Zappa/
