Title: GrandPy Bot in «serverless» style
Date: 2019-03-16 23:34
Summary: Evolution of project 7 realized during the OpenClassrooms course with : Zappa, AWS Lambda & Gitlab-CI/Pages.
Category: Réalisations
Lang: en
Slug: grandpy-bot-zappa-flask-lambda-gitlab
Status: Published
Tags: zappa, flask, aws, aws-lambda, gitlab-ci, gitlab-pages, devops, ci, cd, ci-cd, git, bot, api-gateway, python

This was a [student project][oc] previously hosted on [Github][ocp7]. I updated it to play with :

* [Zappa][zappa]
* [Flask][flask]
* [AWS Lambda][awslmbd]
* [Gitlab-ci][glci]

It is a _robot_ grandfather talking about geographic memories and quoting Wikipedia fetching data over public APIs.

If you can speak French, [have a talk with him][prod] (^_^)

Run yours !
-----------

Need some environment variables to run :

- `EMAIL_ADDRESS` (Nominatim API)
- `MAPBOX_API_KEY` (Mapbox API)
- `GOO_API_KEY` (not used [but needed][issue51])


### Locally :

It works as a simple single-page [Flask][flask] application :

- [fork-it][fork]
- set up a virtualenv : `virtualenv .venv`
- install requirements : `pip install -r requirements.txt`
- run it locally : `python run.py`

### On Gitlab :

Set up a [AWS IAM profile][awsiam] on **your AWS account** and add valid environment variables in `https://gitlab.com/<namespace>/grandpy/settings/ci_cd` :

- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`

Push it back and the _CI script_ will :

- put chat code on [AWS Lambda][awslmbd] with the magic of [Zappa][zappa]
- edit [JavaScript][js] with the URL of the _AWS API Gateway_ just updated
- generate the HTML landing page (with a [flask script][genstat])
- deploy statics on _gitlab-pages_ (HTML, images & CSS)

…and _voilà_, have a chat with _GrandPy_ on `https://<namespace>.gitlab.io/grandpy/` (-;

---

Feel free to give feedback using [issues][issues].

[awsiam]: https://github.com/Miserlou/Zappa/blob/master/example/policy/deploy.json
[awslmbd]: https://console.aws.amazon.com/lambda/
[flask]: http://flask.pocoo.org/
[fork]: https://gitlab.com/free_zed/grandpy/forks/new
[genstat]: https://gitlab.com/free_zed/grandpy/blob/master/genstat.py
[glci]: https://gitlab.com/free_zed/grandpy/blob/master/.gitlab-ci.yml
[issue51]: https://gitlab.com/free_zed/grandpy/issues/
[issues]: https://gitlab.com/free_zed/grandpy/issues/new
[js]: https://gitlab.com/free_zed/grandpy/tree/master/static/js/ask.js
[oc]: {filename}/ocp7.md
[ocp7]: https://github.com/freezed/ocp7
[prod]: https://free_zed.gitlab.io/grandpy
[zappa]: https://github.com/Miserlou/Zappa/
