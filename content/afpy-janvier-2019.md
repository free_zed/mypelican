Title: Histoires d'un étudiant perfectionniste sous pression
Date: 2019-01-21 19:48
Category: Conférences
Summary: Ma 1ère présentation publique : La découverte de Django en jouant avec l'API OpenFoodFacts
Status: Published
Tags: afpy, meetup, django, lyon, python, openfoodfacts, présentation


Ma 1ère présentation publique : j'ai voulu partager avec la _communauté Pytholyonnistes_ ma découverte de l'incontournable framework web de l'univers Python : _Django_.
Au programme : partage d'un des moments forts du parcours d'[OpenClassrooms][oc] en bricolant (entre autre) avec les données de la base de donnée publique et ouverte [openfoodfacts.org][off].

Cette [présentation publique][meetup] s'est déroulée dans le cadre des [rencontres  lyonnaises et mensuelles][afpy] de l'AFPY.

Le support visuel de cette présentation est disponible en cliquant **sur le logo** de _l'AFPy Lyon_ ci-dessous.

[![logo AFPy Lyon][afpyimg]][support]

![AFPy Lyon][photo]


[afpy]: https://www.meetup.com/fr-FR/Python-AFPY-Lyon/
[afpyimg]: {static}/img/afpylyon-200.png
[meetup]: https://www.meetup.com/fr-FR/Python-AFPY-Lyon/events/pvlzcpyzcbnc/
[oc]: https://openclassrooms.com/fr/paths/68-developpeur-dapplication-python
[off]: https://world-fr.openfoodfacts.org/decouvrir
[support]: {attach}/pdf/20190123-presentation-fred-zind-afpy.pdf
[photo]: {attach}/img/20190123-191546-meetup-afpy-lyon-cordee-low.jpg
