Title: Sauvegarder la config du bureau MATE
Date: 2019-03-05 20:05
Summary: Configurer son bureau en quelques commande pour se sentir chez soit partout.
Category: Bloc-notes
Tags: backup, mate, debian, linux, admin, lmde2
Status: Published


Sauvegarde de la BDD _MATE_ avec [`dconf`][dconf] :

* `dconf dump / > ~/Desktop/dconf-backup`

Un peu de tri dans les réglages que l'on souhaite garder :

* `vi ~/Desktop/dconf-backup`

Puis sauvegarder des icônes & thèmes :

* `tar -czvf icons-backup.tar.gz ~/.icons`
* `tar -czvf themes-backup.tar.gz ~/.themes`

On restore la config dans le bureau de destination :

* `dconf load / < dconf-backup`
* `tar -xzvf icons-backup.tar.gz -C ~/`
* `tar -xzvf themes-backup.tar.gz -C ~/`

---
Sources :

* [addictivetips.com] : [_How do I restore MATE panel settings from old backup_][at]
* [askubuntu.com] : [_How To Back Up The Mate Desktop Settings On Linux_][so]

[dconf]: https://packages.debian.org/source/buster/dconf
[at]: https://www.addictivetips.com/ubuntu-linux-tips/back-up-the-mate-desktop-settings-linux/
[so]: https://askubuntu.com/a/821571/836292
