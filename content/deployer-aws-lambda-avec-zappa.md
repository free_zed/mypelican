Title: Déploiement sur AWS Lambda avec Zappa & Flask
Date: 2019-02-11 21:12
Category: Bloc-notes
Status: Published
Summary: Déploiement d'AWS Lambda avec Zappa
Tags: aws, lambda, serverless, flask, zappa, devops, python, api-gateway, aws-lambda

Mise en place d'un **bac à sable**. L'idée est de jouer avec l'approche [_server-less_][serverless] dans un contexte **100% python** en utilisant [Zappa][zappa] associé a une application [Flask][flask] dans le but de déployer le code sur [AWS Lambda][aws-lbd]. Les essais de code et les différants déploiements réalisés sont disponibles sur le dépôt [Gitlab][repo].


AWS
---

1. [créer un compte][aws-account] AWS.
2. créer [une stratégie IAM][aws-iam-strat]
    * s'inspirer de la [discussion][zappa-disc] et du [template][zappa-tmplt] du dépôt `zappa`
3. ajouter cette stratégie [à un groupe][aws-iam-grp]
4. avoir un fichier local contenant un [accès AWS _conforme_][aws-cred]. Le [CLI AWS][aws-cli] peut aider.


Python
------

1. un virtualenv avec `python3.6` <del>ou `python2.7`</del>
    * sous _Debian testing/buster_, [la version de virtualenv fournie par `apt` n'est plus utilisable][so-py36]
2. `source .venv/bin/activate`
3. `pip install flask zappa`
4. faire une petite application [Flask][flask]

```python
from flask import Flask
app = Flask(__name__)

    @app.route('/', defaults={'url_param': 'world'})
    @app.route('/<string:url_param>')
    def index(url_param):
        return "Hello {}. Zappa rocks!".format(url_param)
```

La magie Zappa
--------------

* `zappa init`
```
███████╗ █████╗ ██████╗ ██████╗  █████╗
╚══███╔╝██╔══██╗██╔══██╗██╔══██╗██╔══██╗
  ███╔╝ ███████║██████╔╝██████╔╝███████║
 ███╔╝  ██╔══██║██╔═══╝ ██╔═══╝ ██╔══██║
███████╗██║  ██║██║     ██║     ██║  ██║
╚══════╝╚═╝  ╚═╝╚═╝     ╚═╝     ╚═╝  ╚═╝

Welcome to Zappa!

Zappa is a system for running server-less Python web applications on AWS Lambda and AWS API Gateway.
This `init` command will help you create and configure your new Zappa deployment.
Let's get started!

Your Zappa configuration can support multiple production stages, like 'dev', 'staging', and 'production'.
What do you want to call this environment (default 'dev'):

AWS Lambda and API Gateway are only available in certain regions. Let's check to make sure you have a profile set up in one that will work.
We found the following profiles: adminuser, fred-cli-lambda, and default. Which would you like us to use? (default 'default'):

Your Zappa deployments will need to be uploaded to a private S3 bucket.
If you don't have a bucket yet, we'll create one for you too.
What do you want to call your bucket? (default 'zappa-664wolx7u'):

It looks like this is a Flask application.
What's the modular path to your app's function?
This will likely be something like 'your_module.app'.
We discovered: run.app
Where is your app's function? (default 'run.app'):

You can optionally deploy to all available regions in order to provide fast global service.
If you are using Zappa for the first time, you probably don't want to do this!
Would you like to deploy this application globally? (default 'n') [y/n/(p)rimary]:

Okay, here's your zappa_settings.json:

{
    "dev": {
        "app_function": "run.app",
        "aws_region": "eu-west-3",
        "profile_name": "default",
        "project_name": "hellozappa",
        "runtime": "python3.6",
        "s3_bucket": "zappa-664wolx7u"
    }
}

Does this look okay? (default 'y') [y/n]:

Done! Now you can deploy your Zappa application by executing:

	$ zappa deploy dev

After that, you can update your application code with:

	$ zappa update dev

To learn more, check out our project page on GitHub here: https://github.com/Miserlou/Zappa
and stop by our Slack channel here: https://slack.zappa.io

Enjoy!,
 ~ Team Zappa!
```
* `zappa deploy dev`

=> En ligne !
-------------

Plus de code et d'info sur le dépôt [Gitlab][repo].


[aws-account]: https://portal.aws.amazon.com/billing/signup#/start
[aws-cli]: https://docs.aws.amazon.com/fr_fr/cli/latest/userguide/cli-chap-configure.html
[aws-cred]: https://aws.amazon.com/fr/blogs/security/a-new-and-standardized-way-to-manage-credentials-in-the-aws-sdks/
[aws-iam-grp]: https://console.aws.amazon.com/iam/home#/groups$new
[aws-iam-strat]: https://console.aws.amazon.com/iam/home#/policies$new
[aws-iam-usr]: https://console.aws.amazon.com/iam/home#/users$new
[aws-lbd]: https://console.aws.amazon.com/lambda/ "AWS Lambda lets you run code without provisioning or managing servers"
[flask]: http://flask.pocoo.org "Flask : web developpement one drop at a time"
[repo]: http://gitlab.com/free_zed/hellozappa/
[serverless]: https://en.wikipedia.org/wiki/Serverless_computing
[so-py36]: https://unix.stackexchange.com/a/499965
[zappa-disc]: https://github.com/Miserlou/Zappa/issues/244
[zappa]: https://github.com/Miserlou/Zappa/#zappa---serverless-python "Build and deploy server-less, event-driven Python applications"
[zappa-tmplt]: https://github.com/Miserlou/Zappa/blob/master/example/policy/deploy.json
