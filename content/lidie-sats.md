Title: Pilotage de projet de transition numérique
Date: 2011-04-21 11:11
Category: Réalisations
Status: Published
Summary: Gestion d'un projet de transition numérique : d'une facture en papier vers une appli android
Tags: android, digitalisation, mysql, admin, innovation

Entre 2007 et 2017, en parallèle de mon poste de responsable d'exploitation dans une PME de dépannage automobile, j'étais en charge du pilotage de la transition numérique de notre outils de facturation.

<iframe width="560" height="315" src="https://www.youtube.com/embed/xQpbNvYRbV0" frameborder="0"></iframe>
