#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Fred'
DEFAULT_LANG = 'fr'
DEFAULT_PAGINATION = False
PATH = 'content'
SITEURL = 'http://127.0.0.1:8000'
RELATIVE_URLS = False
SITENAME = 'pro[zind]'
SITESUBTITLE = ''
TIMEZONE = 'Europe/Paris'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Archives', '/articles/'),
         ('Tags', '/tags/'),
         ('Mentions légales', '/mentions-legales/'),)

# Social widget
SOCIAL = (('gitlab', 'https://gitlab.com/free_zed'),
          ('stackoverflow', 'https://stackoverflow.com/users/6709630/freezed'),
          ('github', 'https://github.com/freezed/'),)

# my configuration
DELETE_OUTPUT_DIRECTORY = True
OUTPUT_PATH = 'public/'

STATIC_PATHS = [
    'extra',
]
EXTRA_PATH_METADATA = {
    'extra/robots.txt': {'path': 'robots.txt'},
    'extra/favico.ico': {'path': 'favico.ico'},
}
FAVICO = EXTRA_PATH_METADATA['extra/favico.ico']['path']
DISPLAY_CATEGORIES_ON_MENU = True
THEME = 'mytheme/'
DEFAULT_METADATA = {
    'Status': 'draft',
    'Category': 'Bloc-notes',
}

# URL building
ARCHIVES_SAVE_AS = 'articles/index.html'
ARCHIVES_URL = 'articles/'
ARTICLE_LANG_SAVE_AS = 'articles/{date:%Y}/{date:%m}/{slug}/{lang}/index.html'
ARTICLE_LANG_URL = 'articles/{date:%Y}/{date:%m}/{slug}/{lang}/'
ARTICLE_SAVE_AS = 'articles/{date:%Y}/{date:%m}/{slug}/index.html'
ARTICLE_URL = 'articles/{date:%Y}/{date:%m}/{slug}/'
AUTHOR_URL = 'a-propos/'
CATEGORY_SAVE_AS = '{slug}/index.html'
CATEGORY_URL = '{slug}/'
PAGE_LANG_SAVE_AS = '{slug}/{lang}/index.html'
PAGE_LANG_URL = '{slug}/{lang}/'
PAGE_SAVE_AS = '{slug}/index.html'
PAGE_URL = '{slug}/'
TAGS_SAVE_AS = 'tags/index.html'
TAGS_URL = 'tags/'
TAG_SAVE_AS = 'tags/{slug}/index.html'
TAG_URL = 'tags/{slug}/'
YEAR_ARCHIVE_SAVE_AS = 'articles/{date:%Y}/index.html'
YEAR_ARCHIVE_URL = 'articles/{date:%Y}/'

# URL of the repo to link build commit
REPO_COMMIT_URL = 'http://git.localhost/repo/commit'

# Max number of tags displayed in achives
# Used in template `taglist_sort.html`
MAX_NB_TAG_IN_LIST = 8
